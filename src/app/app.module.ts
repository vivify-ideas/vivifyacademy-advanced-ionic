import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { AddTodoPage } from '../pages/add-todo/add-todo';
import { HomePage } from '../pages/home/home';
import { TodoModal } from '../pages/todo-modal/todo-modal';
import { TodoItemPage } from '../pages/todo-item/todo-item';
import { TodoService } from '../shared/todo.service';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AddTodoPage,
    TodoItemPage,
    TodoModal
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AddTodoPage,
    TodoItemPage,
    TodoModal
  ],
  providers: [
    StatusBar,
    SplashScreen,
    TodoService,
    Network,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
