import { Component, ViewChild } from '@angular/core';
import { AlertController, ModalController, Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';

import { AddTodoPage } from '../pages/add-todo/add-todo';
import { HomePage } from '../pages/home/home';
import { TodoModal } from '../pages/todo-modal/todo-modal';
import { TodoService } from '../shared/todo.service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(
    public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen,
    private alertCtrl: AlertController,
    private network: Network,
    private todoService: TodoService,
    private modalCtrl: ModalController
  ) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Add Todo', component: AddTodoPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.showTodos();

      if (this.network.type === 'none') {
        this.alertCtrl.create({
          title: 'Network problem',
          subTitle: 'Vi nemate mreze na vasem uredjaju',
        }).present();
      }
    });
  }

  showTodos() {
    this.todoService.getAll().then(todos => {
      let date = new Date();
      let currentTime = date.getTime();
      date.setMinutes(date.getMinutes() + 30);
      let futureTime = date.getTime();
      
      todos = todos.filter((todo) => {
        const todoTime = new Date(`${todo.date} ${todo.time}`).getTime();
        return todoTime > currentTime && todoTime < futureTime;
      });

      this.showTodoAlert(todos);
    });    
  }

  showTodoAlert(todos) {
    if (todos.length > 0) {
      const profileModal = this.modalCtrl.create(TodoModal, { todos });
      profileModal.present();
    }
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
