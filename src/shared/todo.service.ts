import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class TodoService {
  constructor(
    private storage: Storage
  ) {}
  
  push(item) {
    return this.storage.get('todos').then((todos) => {
      return todos || [];
    }).then((todos) => {
      todos.push(item);
      this.storage.set('todos', todos);
    });
  }

  getAll() {
    return this.storage.get('todos').then((todos) => {
      return todos || [];
    }).then((todos) => {
      return todos;
    })
  }
}