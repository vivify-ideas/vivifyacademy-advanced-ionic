import { HomePage } from '../home/home';
import { Component } from '@angular/core';
import { AlertController, NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-todo-item',
  templateUrl: 'todo-item.html'
})

export class TodoItemPage {
  todo: {title: string, date: string, time: string, done: boolean};

  constructor(
      public navCtrl: NavController, 
      public navParams: NavParams,
      public alertController: AlertController
  ) {
    this.todo = this.navParams.get('todo');
  }

  close() {
    this.navCtrl.pop();
  }
}

