import { TodoItemPage } from '../todo-item/todo-item';
import { TodoService } from '../../shared/todo.service';
import { Component } from '@angular/core';
import { AlertController, ModalController, NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  selectedItem: any;
  items: Array<{title: string, date: string, time: string, done: boolean}>;
  icons: string[];

  constructor(
      public navCtrl: NavController, 
      public navParams: NavParams,
      public alertController: AlertController,
      public todoService: TodoService,
      public modalCtrl: ModalController
  ) {
    // If we navigated to this page, we will have an item available as a nav param
    this.selectedItem = navParams.get('item');
    this.todoService.getAll().then((todos) => {
      this.items = todos;
    });
  }

  showTodo(todo) {
    const profileModal = this.modalCtrl.create(TodoItemPage, { todo });
    profileModal.present();
  }
}

