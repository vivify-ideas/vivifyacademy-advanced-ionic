import { HomePage } from '../home/home';
import { TodoService } from '../../shared/todo.service';
import { Component } from '@angular/core';
import { AlertController, NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-add-todo',
  templateUrl: 'add-todo.html'
})

export class AddTodoPage {
  title: string;
  date: string;
  time: string;

  constructor(
      public navCtrl: NavController, 
      public navParams: NavParams,
      public alertController: AlertController,
      public todoService: TodoService
  ) {
  }

  addTodo() {
    this.todoService.push({
      title: this.title,
      date: this.date,
      time: this.time,
      done: false
    }).then(() => {
      this.showSuccessMessage();
      this.navCtrl.push(HomePage);
    });
  }
  
  showSuccessMessage() {
    this.alertController.create({
      title: 'Uspesno',
      subTitle: 'Uspesno ste kreirali item',
      buttons: ['Dismiss']
    }).present();
  }
}

