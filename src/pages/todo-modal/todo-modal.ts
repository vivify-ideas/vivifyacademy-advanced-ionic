import { TodoService } from '../../shared/todo.service';
import { HomePage } from '../home/home';
import { Component } from '@angular/core';
import { AlertController, NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'todo-modal',
  templateUrl: 'todo-modal.html'
})

export class TodoModal {
  items: Array<{title: string, date: string, time: string, done: boolean}>;

  constructor(
      public navCtrl: NavController, 
      public navParams: NavParams,
      public alertController: AlertController,
      private todoService: TodoService
  ) {
    this.items = this.navParams.get('todos');
  }

  close() {
    this.navCtrl.pop();
  }
}

